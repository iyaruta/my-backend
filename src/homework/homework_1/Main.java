package homework.homework_1;

import java.util.Random;
import java.util.Scanner;

public class Main {

    private static String name;

    public static void main(String[] args) {
        System.out.println("Enter you name");
        Scanner scanner = new Scanner(System.in);
        name = scanner.next();
        System.out.println("Hi " + name + " Let the game begin");
        int playnumber = scanner.nextInt();
        int rando = random();
        check(playnumber, rando);
    }

    public static void check(int playnumber, int random) {
        Scanner scanner = new Scanner(System.in);
        while (playnumber != random) {
            while (playnumber < random) {
                System.out.println("Your number is too small. Please, try again.");
                playnumber = scanner.nextInt();
            }
            while (playnumber > random) {
                System.out.println("Your number is too big. Please, try again.");
                playnumber = scanner.nextInt();
            }
        }
        System.out.println("Congratulations, " + name + "!");
    }

    public static int random() {
        int min = 1;
        int max = 100;
        int diff = max - min;
        Random random = new Random();
        int i = random.nextInt(diff + 1);
        return i += min;
    }
}
