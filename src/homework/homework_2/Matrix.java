package homework.homework_2;

public class Matrix {

    public static void main(String[] args) {

        int[][] matrixA;
        matrixA = new int[6][6];

        matrixA[0][0] = 0;
        matrixA[0][1] = 1;
        matrixA[0][2] = 2;
        matrixA[0][3] = 3;
        matrixA[0][4] = 4;
        matrixA[0][5] = 5;
//        matrixA[1][0] = 1;
        matrixA[1][0] = 1;
        matrixA[2][0] = 2;
        matrixA[3][0] = 3;
        matrixA[4][0] = 4;
        matrixA[5][0] = 5;

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                System.out.print(matrixA[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
