package homework.homework_4.HeppyFamily;

public class Main {

    public static void main(String[] args) {
        System.out.println(getFamilyMichael());
        System.out.println(getFamilyJohan());

    }

    public static Human getFamilyJohan() {
        Pet pet = new Pet("Dog", "Jek");

        String[][] habits = new String[1][0];
        Human mother = new Human("Scarlet", "johansson", 20);
        mother.setPet(pet);
        Human father = new Human("Mike", "johansson", 25);
        father.greetPet();
        father.describePet();
        Human human = new Human("Johan","johansson", 1988, mother, father);
        return human;
    }

    public static Human getFamilyMichael() {
        String[] habitsPet = {"eat", "drink", "sleep"};
        Pet pet = new Pet("Dog", "Rock", 5, 75, habitsPet);

        String[][] habits = new String[1][0];
        Human mother = new Human("Jane", "Karleone", 30);
        Human father = new Human("Vito", "Karleone", 35);
        Human human = new Human("Michael", "Karleone", 1977, 90, pet, mother, father, habits);
        return human;
    }
}
