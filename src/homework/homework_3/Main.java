package homework.homework_3;

import java.util.Scanner;

public class Main {

    private static String[][] dayWeek = dayWeek();

    private static String[][] dayWeek() {
        String[][] schedule = new String[7][2];

        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesdays";
        schedule[2][1] = "eat pizza with my family";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "go to courses; do home work";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "go to sleep";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to relax";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "go to my parens";
        return schedule;
    }

    public static void main(String[] args) {
        String enterDay = null;
        while (!"exit".equals(enterDay)) {
            System.out.println("Please, input the day of the week:");
            Scanner scanner = new Scanner(System.in);
            enterDay = scanner.nextLine();
            for (int i = 0; i < dayWeek.length; i++) {
                if (dayWeek[i][0].equalsIgnoreCase(enterDay.trim())) {
                    String line = String.join(": ", dayWeek[i]);
                    System.out.println("Your tasks for " + line);
                    break;
                } else if (!"exit".equals(enterDay) && i == dayWeek.length - 1) {
                    System.out.println("Sorry, I don't understand you, please try again");
                }
            }
        }
    }
}